from django.urls import path

from ToDo.views import task_list, add_task

urlpatterns = [
    path('task_list', task_list, name='task_list'),
    path('add_task', add_task, name='add_task')
]
