from django.db import models


# Create your models here.

class User(models.Model):
    class Meta:
        db_table = 'user'

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dni = models.IntegerField(unique=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    email = models.EmailField(unique=True)
    birthday = models.DateField()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Task(models.Model):
    class Meta:
        db_table = 'task'

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    name_task = models.CharField(max_length=50)
    description = models.TextField()
    status = models.BooleanField(default=False)
    date_start = models.DateTimeField(default=None)
    date_end = models.DateTimeField(default=None)

    def __str__(self):
        return self.name_task
