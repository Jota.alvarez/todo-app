from django.shortcuts import render, redirect

# Create your views here.
from ToDo.forms import TaskForm
from ToDo.models import Task, User


def task_list(request):
    context = Task.objects.filter(user_id=request.user.pk)
    return render(request, 'task/task_list.html', {'context': context})


def add_task(request):
    template = 'task/add_task.html'
    if request.method == 'POST':
        form = TaskForm(request.POST or None)
        if form.is_valid():
            task = form.save(commit=False)
            user_id = form.cleaned_data.get('users')
            task.user_id = User.objects.get(pk=user_id)
            task.save()
            return redirect('task_list')
    else:
        form = TaskForm()

    return render(request, template, {'form': form})
