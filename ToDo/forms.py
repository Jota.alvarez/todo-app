from django import forms

from ToDo.models import Task, User


class TaskForm(forms.ModelForm):

    users = forms.ModelChoiceField(queryset=User.objects.all())
    name_task = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))
    status = forms.BooleanField(widget=forms.CheckboxInput(), required=False)
    date_start = forms.DateTimeField(widget=forms.DateTimeInput())
    date_end = forms.DateTimeField(widget=forms.DateTimeInput())

    class Meta:
        model = Task
        fields = [
                    'name_task', 'description', 'status', 'date_start', 'date_end',
                  ]
